<?php

/*
  Plugin Name: File Fingerprint
  Plugin URI: http://lostiumproject.com
  Version: 1.0
  Author: Lostium Project
  Author uri: http://lostiumproject.com
  Description: Append MD5 checksum to filename.
  Text Domain: FileFingerprint
  Domain Path: /languages
 */

namespace lostium\filefingerprint;

/**
 * Esta clase se encarga de incorporar un hash md5 al final del nombre del fichero, de esta forma podemos establecer una caché de ficheros de recursos a largo plazo
 * ya que nunca se subirá el mismo fichero
 */
class FileFingerprint {
    
    public function __construct() {
        add_filter('wp_handle_upload_prefilter', array($this, 'wp_handle_upload_prefilter'));
    }

    public function wp_handle_upload_prefilter($file) {
        if (!$file['error']) {
            //Calculamos el md5 del fichero
            $md5 = md5_file($file['tmp_name']);

            $fileInfo = pathinfo($file['name']);

            $file['name'] = sanitize_title($fileInfo['filename'] . '-' . $md5) . '.' . sanitize_title($fileInfo['extension']);

        }
       return $file;
    }

}

global $fileFingerPrint;

$fileFingerPrint = new \lostium\filefingerprint\FileFingerprint();
